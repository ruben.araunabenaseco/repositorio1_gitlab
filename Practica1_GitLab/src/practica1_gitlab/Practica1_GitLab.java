/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica1_gitlab;

/**
 * Entornos de Desarrollo
 * Practila1_GitLab
 * 09/11/2021
 * Autor: Rubén Araunabeña Seco
 * 
 */
public class Practica1_GitLab {

    // Este es el maindel repositorio clonado de GitLab.
    public static void main(String[] args) {
        System.out.println("Mi primer commit desde NetBeans");
        System.out.println("Commit desde GitLab");
    }
    
}
